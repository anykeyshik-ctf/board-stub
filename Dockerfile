FROM nginx:alpine

COPY ./page /stub
COPY ./http.conf /etc/nginx/nginx.conf
COPY ./ssl /ssl

EXPOSE 80
EXPOSE 443
